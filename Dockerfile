FROM detochko/vscode-ssh-dev:2.2

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
	curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
	echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
	apt-get update && \
	apt-get install -y --no-install-recommends \
	nodejs yarn && \
	yarn config set cache-folder /tmp/.yarn && \
	npm config set cache /tmp/.npm --global && \
	apt-get autoremove -y && \
	apt-get clean -y && \
	rm -rf /var/lib/apt/lists/*

VOLUME /app /tmp
WORKDIR /app